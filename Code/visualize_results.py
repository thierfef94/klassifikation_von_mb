import pandas as pd
import numpy as np
import joblib
import matplotlib.pyplot as plot
import seaborn as sns
import math
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from ml_methods import get_metrics, get_metrics_cv, get_metrics_evaluation
from sklearn.decomposition import PCA
from sklearn.metrics import classification_report


###########################         PLOTS        ###########################

def classes_dist_plot(classes,filename):
    fig, ax = plot.subplots(1,5,figsize=(10,6), sharey= True, gridspec_kw={'width_ratios': [1,1,2.5,1,1]})
    fig.subplots_adjust(wspace = 0)
    data = pd.melt(classes, 'index').dropna()
    data.value = data.value.astype('int64')
    annos = [['negativ','positiv'],['negativ','positiv'],['MB, G3','MB, G4','MB, SHH CHL AD','MB, SHH INF','MB, WNT'],['negativ','positiv'],['negativ', 'positiv']]
    for i, c in enumerate(data.variable.unique()):
        df = data[data.variable==c]
        sns.barplot( x='index', 
                y = 'value',
                hue = 'index',
                data = df,
                ax = ax[i])
        handles, labels = ax[i].get_legend_handles_labels()
        if (c == 'osstatus') | (c == 'os'): c = 'Gesamt-\nüberleben'
        if c == 'm': c = 'Metastasierung'
        if c == 'v11': c = 'molekulare Subgruppe'
        if c == 'TERT': c = 'TERT-Mutation'
        if c == 'TP53': c = 'TP53-Mutation'
        ax[i].set_ylabel('')
        ax[i].set_xlabel('')
        min_v = 0
        max_v = 1*len(df)
        ax[i].set_xlim(min_v,max_v)
        if i != 0: ax[i].set_yticks([])
        ax[i].set_title('')
        ax[i].legend([],[], frameon=False)
        # min_v = ax[i].get_xbound()[0]
        # max_v = ax[i].get_xbound()[1]-0.5
        ax[i].set_xticks([(max_v-min_v)/2])
        ax[i].set_xticklabels([c])
        for j,p in enumerate([l for l in ax[i].patches if not( math.isnan(l.get_height()))]):
            new_width = 0.7
            new_pos = min_v-(0.7/8)+j*(max_v-0.35-min_v)/(len([l for l in ax[i].patches if not( math.isnan(l.get_height()))]))
            # we change the bar width
            p.set_width(new_width)
            # we recenter the bar
            p.set_x(new_pos + p.get_width() * .5)
        for p,t in zip([l for l in ax[i].patches if not( math.isnan(l.get_height()))], annos[i]):
            _x = p.get_x() + p.get_width() /2
            _y = 20
            value = t+': '+str(int(p.get_height()))
            ax[i].text(_x, _y, value, ha="center", rotation = 90) 

    ax[0].set_ylabel('Anzahl der Fälle')
    ax[2].set_xlabel('Zielvariable')
    ax[2].set_title('Verteilung innerhalb der Zielvariable')
    sns.despine(right = True)
    fig.savefig(filename)
    plot.close(fig)
    return data

def model_plot(model, filename, sampling = False):
    fig, ax = plot.subplots(figsize=(10,6))
    m_nr = model[0]['Set'].split('Modell')[1]
    if (not sampling): scores = [i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]] for i in model if ('sanity' not in i['Set']) & ('under' not in i['Set']) & ('over' not in i['Set'])]
    if (sampling): scores = [i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]] for i in model if 'sanity' not in i['Set']]
    data = pd.DataFrame(scores, columns = list(range(0,10))+['class','method'])
    sns.boxplot( x = 'class',
                    y = 'value',
                    data = pd.melt(data, id_vars = ['class','method']),
                    hue = 'method',
                    ax = ax)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, ['RF','SVM','LR'])
    ax.set(ylim=(0.4,1))
    ax.set_ylabel('Balanced Accuracy')
    ax.set_xlabel('Zielvariable')
    ax.set_xticklabels(['GÜ','mol.Sub.','M','TERT-M','TP53-M'])
    ax.set_title('Vorhersage mit Modell '+m_nr)
    sns.despine(right = True)
    fig.savefig(filename)
    plot.close(fig)
    return data

def model_plot_sanity(model, filename):
    fig, ax = plot.subplots(figsize=(10,6))
    m_nr = model[0]['Set'].split('Modell')[1]
    scores = [i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]] if 'sanity' not in i['Set'] else i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]+'_'+i['Set'].split('_')[-3]] for i in model]
    data = pd.DataFrame(scores, columns = list(range(0,10))+['class','method'])
    sns.boxplot( x = 'class',
                    y = 'value',
                    data = pd.melt(data, id_vars = ['class','method']),
                    hue = 'method',
                    ax = ax)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, ['RF','SVM','LR','sanity_rf','sanity_svm'])
    ax.set(ylim=(0.4,1))
    ax.set_ylabel('Balanced Accuracy')
    ax.set_xlabel('Zielvariable')
    ax.set_xticklabels(['GÜ','mol.Sub.','mol.Sub.','M','TERT-M','TP53-M'])
    ax.set_title('Vorhersage mit Modell '+m_nr)
    sns.despine(right = True)
    fig.savefig(filename)
    plot.close(fig)
    return data

def class_plot_sanity(model, filename):
    fig, ax = plot.subplots(figsize=(10,6))
    m_nr = model[0]['Set'].split('Modell')[1]
    scores = [i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]] if 'sanity' not in i['Set'] else i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]+'_'+i['Set'].split('_')[-3]] for i in model]
    data = pd.DataFrame(scores, columns = list(range(0,10))+['class','method'])
    sns.boxplot( x = 'class',
                    y = 'value',
                    data = pd.melt(data, id_vars = ['class','method']),
                    hue = 'method',
                    ax = ax)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, ['RF','SVM','LR','sanity_rf','sanity_svm'])
    ax.set(ylim=(0.3,1))
    ax.set_ylabel('Balanced Accuracy')
    ax.set_xlabel('Zielvariable')
    ax.set_xticklabels(['GÜ','mol.Sub.','mol.Sub.','M','TERT-M','TP53-M'])
    ax.set_title('Vorhersage mit Modell '+m_nr)
    sns.despine(right = True)
    fig.savefig(filename)
    plot.close(fig)
    return data

def cn_plot(data, cutoff, filename = None):
    cutoff = float(cutoff)
    fig, ax = plot.subplots(figsize=(10,6))
    if (cutoff >= 0): data = data.apply(lambda x: x >= cutoff).sum().sort_values(ascending=False).apply(lambda x : (x/data.shape[0])*100)
    if (cutoff < 0): data = data.apply(lambda x: x <= cutoff).sum().sort_values(ascending=False).apply(lambda x : (x/data.shape[0])*100)
    sns.barplot(y = data.index.tolist(),
                x = data,
                ax = ax)
    ax.set_ylabel('Northcott Gene')
    if (cutoff >= 0): 
        sns.despine(right = True)
        ax.set_xlabel('% der Fälle mit Amplifikation')
    if (cutoff < 0): 
        sns.despine(left = True, right = False)
        ax.set_xlabel('% der Fälle mit Verlust')
        ax.yaxis.tick_right() 
        ax.yaxis.set_label_position("right")
        ax.invert_xaxis()
    if filename != None: fig.savefig(filename)
    plot.close(fig)
    return data

def model_plot2(model, filename, sampling = False):
    fig, ax = plot.subplots(figsize=(10,6))
    m_nr = model[0]['Set'].split('Modell')[1]
    if (not sampling): scores = [i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]] for i in model if ('sanity' not in i['Set']) & ('under' not in i['Set']) & ('over' not in i['Set'])]
    if (sampling): scores = [i['scores']+[i['Set'].split('_')[0],i['Set'].split('_')[-2]] for i in model if 'sanity' not in i['Set']]
    data = pd.DataFrame(scores, columns = list(range(0,10))+['class','method'])
    sns.boxplot( x = 'class',
                    y = 'value',
                    data = pd.melt(data, id_vars = ['class','method']),
                    hue = 'method',
                    ax = ax)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, ['RF','SVM','LR'])
    ax.set(ylim=(0.4,1))
    ax.set_ylabel('Balanced Accuracy')
    ax.set_xlabel('Zielvariable')
    ax.set_xticklabels(['GÜ','mol.Sub.','M','TERT-M','TP53-M'])
    ax.set_title('Vorhersage mit Modell '+m_nr)
    sns.despine(right = True)
    fig.savefig(filename)
    plot.close(fig)
    return data

def pred_boxplot(models, filename, sampling = False):
    c = models[0]['Set'].split('_')[0]
    if (c == 'osstatus') | (c == 'OS'): c = '"Gesamtüberleben"'
    if c == 'M': c = '"Metastasierung"'
    if c == 'v11': c = '"molekulare Subgruppe"'
    if c == 'TERT': c = '"TERT-Mutation"'
    if c == 'TP53': c = '"TP53-Mutation"'
    if (not sampling): scores = [i['Outer_Scores']+[i['Set'].split('_')[-1],i['Set'].split('_')[-2]] for i in models if ('sanity' not in i['Set']) & ('under' not in i['Set']) & ('over' not in i['Set'])]
    if (sampling): scores = [i['Outer_Scores']+[i['Set'].split('_')[-1],i['Set'].split('_')[-2]] for i in models if ('sanity' not in i['Set'])]
    data = pd.DataFrame(scores, columns = list(range(0,10))+['model','method'])
    data_in = pd.melt(data, id_vars = ['model','method'])
    fig, ax = plot.subplots(figsize=(10,6))
    fig, ax = do_boxplot(fig, ax, data_in, c)
    fig.savefig(filename, bbox_inches = 'tight', pad_inches = .1)
    plot.close(fig)
    return data

def sampling_plot(models, filename):
    fig, axs = plot.subplots(figsize=(10,18), nrows = 3, sharex=True)
    c = models[0]['Set'].split('_')[0]
    if (c == 'osstatus') | (c == 'OS'): c = 'Gesamtüberleben'
    if c == 'M': c = 'Metastasierung'
    if c == 'v11': c = 'molekulare Subgruppe'
    if c == 'TERT': c = 'TERT-Mutation'
    if c == 'TP53': c = 'TP53-Mutation'
    scores = [i['Outer_Scores']+[i['Set'].split('_')[-3],i['Set'].split('_')[-1],i['Set'].split('_')[-2]] for i in models if ('sanity' not in i['Set'])]
    data = pd.DataFrame(scores, columns = list(range(0,10))+['sampling','model','method'])
    samp = pd.melt(data, id_vars = ['sampling','model','method'])
    for i,j in zip(samp.groupby('sampling'),axs):
        method = i[0]
        if method == 'over': method = 'mit SMOTE Oversampling'
        elif method == 'under': method = 'mit Random Undersampling'
        else : method = 'ohne Samplingmethode'
        fig, axs = do_boxplot(fig, j,  i[1], c, method)
        # sns.boxplot( x = 'model',
        #                 y = 'value',
        #                 data = i[1],
        #                 hue = 'method',
        #                 ax = axs[j])
        # handles, labels = axs[j].get_legend_handles_labels()
        # axs[j].legend(handles, ['RF','SVM','LR', 'KNN'])
        # axs[j].set(ylim=(0.4,1))
        # axs[j].set_ylabel('Balanced Accuracy')
        # axs[j].set_xlabel('Modell')
        # axs[j].set_xticklabels(['Modell 1\n43 CN-Werte','Modell 2\n43 B-Werte; PCA','Modell 3\nKombination of\nM1 and M2','Modell 4\n10000 mv\nCN-Werte','Modell 5\n10000 mv\nB-Werte','Modell 6\nKombination of\nM4 and M5'])
        # axs[j].set_title('Results for class '+c+' '+method)
        # sns.despine(right = True)
    fig.savefig(filename, bbox_inches = 'tight', pad_inches = .1)
    plot.close(fig)
    return data

def do_boxplot(fig, ax, data, c, method = ''):
    sns.boxplot( x = 'model',
                    y = 'value',
                    data = data,
                    hue = 'method',
                    ax = ax)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, ['RF','SVM','LR', 'KNN'])
    ax.set(ylim=(0.4,1))
    ax.set_ylabel('Balanced Accuracy')
    ax.set_xlabel('Modell')
    ax.set_xticklabels(['Modell 1\n43 CN-Werte','Modell 2\n43 B-Werte; PCA','Modell 3\nKombination von\nM1 und M2','Modell 4\n10000 gr.\nSD CN-Werte','Modell 5\n10000 gr.\nSD B-Werte','Modell 6\nKombination von\nM4 und M5'])
    ax.set_title(('Vorhersage für d. Zielvariable '+c+' '+method).strip())
    for i in range(0,5): ax.axvline(i*1+0.5, 0.05 ,0.95, c = '#dfdfdf', figure = fig)
    sns.despine(right = True)
    return fig, ax

###########################         TABLES        ###########################


### HELPER FUNCTION TO GRAB OUTER SCORES FROM SAVED DATA
def res(dict):
    return [{'Set':i['Set'], 'Outer_Scores':i['Outer_Scores'].tolist()} for i in dict]

### GET MULTIPLE STATISTICS INCLUDING MAX AND MIN ETC
def get_stats(df):
    a = df.describe(percentiles=[]).transpose().describe(percentiles=[])
    return pd.Series(np.diag(a), index=[a.index])

### GET BEST RESULT
def get_best(df):
    return df[df.max().idxmax()].describe(percentiles=[]).drop(['count','50%'])

### GET WORST RESULT
def get_worst(df):
    return df[df.min().idxmin()].describe(percentiles=[]).drop(['count','50%'])

# GET BEST VARIANCE
def get_best_var(df):
    return df[df.var().idxmin()].describe(percentiles=[]).drop(['count','50%'])

#GET WORST VARIANCE
def get_worst_var(df):
    return df[df.var().idxmax()].describe(percentiles=[]).drop(['count','50%'])

#GET BEST MEAN
def get_best_mean(df):
    return df[df.mean().idxmax()].describe(percentiles=[]).drop(['count','50%'])

#HELPER FUNCTION TO SAVE THE TABLE IN XSLX FORMAT WITH SOME COLOR CODING
def save_xls(list_dfs, xls_path):
    with pd.ExcelWriter(xls_path, engine='xlsxwriter') as writer:
        length = [0,0,0]
        width = max(list_dfs.items(), key = lambda x: x[1].shape)[1].shape[1]+1
        for i,j in enumerate(list_dfs.items()):
            if ('over' in j[0]): length[1] = write_in_table(writer, j, length[1], width*1, 'oversampling', '#e8a628')
            elif ('under' in j[0]): length[2] = write_in_table(writer, j, length[2], width*2, 'undersampling', '#f98365')
            else: length[0] = write_in_table(writer, j, length[0], width*0, 'kein sampling', '#f9de59')
        writer.save()
#HELPER FUNCTION TO CREATE THE EXCEL SPREADSHEET
def write_in_table(writer, df, row, column, sampling, color):
    if ('all' in df[0]): row += 1
    df[1].to_excel(writer,'on_one', startrow=row+1, startcol = column)
    df[1].to_excel(writer,'%s' % df[0])
    if ('all' in df[0]): 
        writer.sheets['on_one'].write(row-1,column,sampling, writer.book.add_format({'bold': True, 'fg_color': color}))
        for n in range(1,7): writer.sheets['on_one'].write(row-1, column+n, None, writer.book.add_format({'bold': True, 'fg_color': color}))
    if ('best_model' in df[0]) | ('worst_model' in df[0]) | ('best_sd' in df[0]) | ('worst_sd' in df[0]) | ('best_mean' in df[0]): 
        writer.sheets['on_one'].write(row,column,df[0]+': '+df[1].name, writer.book.add_format({'bold': True, 'fg_color': color}))
    else: writer.sheets['on_one'].write(row,column,df[0], writer.book.add_format({'bold': True, 'fg_color': color}))
    for n in range(1,7): writer.sheets['on_one'].write(row, column+n, None, writer.book.add_format({'bold': True, 'fg_color': color}))

    row += df[1].shape[0]+2
    return row


### CREATE RESULT TABLES WITH DIFFERENT VALUES

def scores_eval(data, filename = None, sampling = None):
    if sampling == None: sampling = ''
    print(sampling)
    df = pd.DataFrame([i['Outer_Scores'] for i in data])
    df = df.transpose()
    df.columns = [i['Set'].lower() for i in data]
    output = dict()
    print('ALL')
    res_all = get_stats(df)
    res_all.name = 'ALLE MODELLE'
    output[('ALLE MODELLE '+sampling).strip()] = res_all
    print(res_all)
    print('BESTES MODELL')
    b = get_best(df)
    print(b)
    print('SCHLECHTESTES MODELL')
    w = get_worst(df)
    print(w)
    print('KLEINSTE VARIANZ')
    bv = get_best_var(df)
    print(bv)
    print('GRÖßTE VARIANZ')
    wv = get_worst_var(df)
    print(wv)
    print('DURCHSCHNITTLICH BESTES MODELL')
    bm = get_best_mean(df)
    print(bm)
    print('NIEDRIG VS HOHE DIM')
    df_cn = df.loc[:,[('model1' in i) | ('model2' in i) | ('model3' in i) for i in df.columns]]
    df_beta = df.loc[:,[('model4' in i) | ('model5' in i) | ('model6' in i) for i in df.columns]]
    res_cb = pd.concat([get_stats(df_cn),get_stats(df_beta)], axis = 1, keys= ['low','high'])
    output[('NIEDRIG VS HOHE DIM '+sampling).strip()] = res_cb
    print(res_cb)
    print('MODELL VS MODELL')
    df_model1 = get_stats(df.loc[:,['model1' in i for i in df.columns]])
    df_model2 = get_stats(df.loc[:,['model2' in i for i in df.columns]])
    df_model3 = get_stats(df.loc[:,['model3' in i for i in df.columns]])
    df_model4 = get_stats(df.loc[:,['model4' in i for i in df.columns]])
    df_model5 = get_stats(df.loc[:,['model5' in i for i in df.columns]])
    df_model6 = get_stats(df.loc[:,['model6' in i for i in df.columns]])
    res_m = pd.concat([df_model1,df_model2,df_model3,df_model4,df_model5,df_model6], axis = 1, keys= ['model1','model2','model3','model4','model5','model6'])
    output[('MODELL VS MODELL '+sampling).strip()] = res_m 
    print(res_m)
    print('METHODE VS METHODE')
    df_rf = get_stats(df.loc[:,['rf' in i for i in df.columns]])
    df_svm = get_stats(df.loc[:,['svm' in i for i in df.columns]])
    df_lr = get_stats(df.loc[:,['lr' in i for i in df.columns]])
    df_knn = get_stats(df.loc[:,['knn' in i for i in df.columns]])
    res_meth = pd.concat([df_rf, df_svm, df_lr, df_knn], axis = 1, keys= ['rf','svm','lr','knn'])
    output[('METHODE VS METHODE '+sampling).strip()]= res_meth
    output[('BESTES MODELL '+sampling).strip()] = b
    output[('SCHLECHTESTES MODELL '+sampling).strip()] = w
    output[('BESTE SD '+sampling).strip()] = bv
    output[('SCHLECHTESTE SD '+sampling).strip()] = wv
    output[('BESTER DURCHSCHNITT '+sampling).strip()] = bm
    print(res_meth)
    if (filename != None):
        save_xls(output, filename)
    return output

### SKLEARN FUNCTIONS TO CREATE A CLASSIFICATION REPORT

def get_metrics(scores, labels, filename = None):
    metrics = {}
    for model in scores:
        report = classification_report(labels.iloc[:,1], model['Predictions'].tolist(), output_dict=True)
        metrics[model['Set']] = pd.DataFrame(report).transpose()
    if (filename != None):
        save_xls(metrics,filename)
    return metrics