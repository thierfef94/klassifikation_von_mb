############################################
###IMPORTS
############################################
import os
from importlib import reload

import joblib
import numpy as np
import pandas as pd
import h5py
# from keras.layers import Dense
# from keras.models import Sequential
# from keras.utils import to_categorical
# from keras.wrappers.scikit_learn import KerasClassifier
from sklearn import metrics, preprocessing, svm
from sklearn.datasets import make_classification  #sampling methods
from sklearn.decomposition import PCA, KernelPCA  #running PCA
from sklearn.ensemble import RandomForestClassifier #RF Class
from sklearn.linear_model import LogisticRegression #LR 

from sklearn.model_selection import (ParameterGrid,
                                    StratifiedKFold)
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelBinarizer, LabelEncoder, StandardScaler

from ml_methods import *
from visualize_results import cn_plot, classes_dist_plot

NUM_GENES = 43
NUM_GENES_FULL = 21211

############################################
###DATA LOADING AND PREPARATION
############################################
### in data frame with beta values
#meta = pd.read_csv('../medullos.csv')
meta = pd.read_excel('../medullo_clinical_snp_felix_150920.xlsx')
meta.rename(columns={'SENTRIXSEL':'idats'},inplace = True)
# betas_mnp = pd.read_csv('../betas_class.csv')
# betas_mnp.drop(columns= 'Unnamed: 0', inplace = True)

h5 = h5py.File('../betas_full.h5', 'r')
h5_betas = pd.DataFrame(np.array(h5['betas'])).transpose()
h5_idats = pd.DataFrame([i.decode('utf-8') for i in np.array(h5['idats']).tolist()[0]], columns = ['idats'])
h5_cpg_names = [i.decode('utf-8') for i in np.array(h5['cpg_names']).tolist()[0]]
betas = pd.concat([h5_idats, h5_betas], axis = 1)
betas.set_axis(['idats']+h5_cpg_names, axis = 1, inplace = True)

del h5, h5_betas, h5_idats, h5_cpg_names

cn = pd.read_csv('../cn_values_full.csv')
cn.drop(columns= ['Unnamed: 0','NA'], inplace = True)
cn_northcott = pd.read_csv('../cn_values_northcott.csv') #Model 1: Klassifikation auf 43 Northcott Genes CNV
cn_northcott.drop(columns= ['Unnamed: 0'], inplace = True)

data = [betas, cn, cn_northcott]

### Nested CV + GRIDSEARCH RF ###
rf_param_grid = {'rf__max_features': ['sqrt'] + [2**i for i in range(0,6)], 'rf__n_estimators': [1000]}
svm_param_grid = {'svm__kernel': ['rbf'], 'svm__gamma': ['auto', 10**-4, 10**-3, 10**-2, 10**-1, 10**0, 10**1]}
knn_param_grid = {'knn__n_neighbors': list(range(1,21))}
lr_param_grid = {}
rf_grid = ParameterGrid(rf_param_grid)
svm_grid = ParameterGrid(svm_param_grid)
knn_grid = ParameterGrid(knn_param_grid)

seed = 1234

# configure the cross-validation procedure
cv_outer = StratifiedKFold (n_splits=10, shuffle=True, random_state= seed)
cv_inner = StratifiedKFold (n_splits=9, shuffle=True, random_state= seed)
pca = PCA(n_components=NUM_GENES, random_state= seed)

#e##########################################
###OVERALL SURVIVAL WORKFLOW
############################################
#overall survival data prep
column = 'osstatus'
data_os_in = meta.dropna(subset =['osyears','osstatus'], axis = 0)
data_os_in.osyears = [1 if i > 5 else 0 for i in data_os_in.osyears.tolist()] # define everything with survival of more than 5 years as 1 and everything else as 0
data_os_in = data_os_in.loc[((data_os_in.osyears != 0) | (data_os_in.osstatus != 0)),]
data_os_in.drop('osyears', axis = 1, inplace = True)
data_os_in = data_os_in[['idats', column]]
data_os_in.to_excel('OS_LABEL.xlsx', index = False)
os_classes = data_os_in.groupby('osstatus').count().iloc[:,0] #count number for each group
data_os = prep_data(data, data_os_in, 'idats')

############################################
###METASTASIS
############################################
# Metastasis data prep
column = 'M'
data_M_in = meta.dropna(subset =[column], axis = 0)
data_M_in = data_M_in[['idats',column]]
data_M_in.to_excel('M_LABEL.xlsx', index = False)
data_M = prep_data(data, data_M_in, 'idats')
m_classes = data_M_in.groupby(column).count().iloc[:,0] #count number for each group

############################################
###V11 METHYLATION GROUP / MOLEKULAR SUBGROUP
############################################
#v11b6_alldiag data prep
column = 'v11b6_alldiag'
data_v11_in = meta.dropna(subset =[column], axis = 0)
data_v11_in = data_v11_in[data_v11_in[column].str.match('MB')]
#renaming groups into simpler labels
encoder = preprocessing.LabelEncoder()
encoder.fit(data_v11_in[column])
data_v11_in[column] = encoder.transform(data_v11_in[column])
data_v11_in = data_v11_in[['idats',column]]
data_v11_in.to_excel('v11_LABEL.xlsx', index = False)
v11_classes = data_v11_in.groupby(column).count().iloc[:,0] #count number for each group
data_v11 = prep_data(data, data_v11_in, 'idats')

############################################
###TP53
############################################
#TP53 data prep
column = 'TP53'
data_TP53_in = meta.dropna(subset =[column], axis = 0)
data_TP53_in = data_TP53_in[['idats',column]]
data_TP53_in.rename(columns = {'TP53':'TP53_Class'}, inplace = True)
column = 'TP53_Class'
data_TP53_in.to_excel('TP53_LABEL.xlsx', index = False)
TP53_classes = data_TP53_in.groupby(column).count().iloc[:,0] #count number for each group before sampling
data_TP53 = prep_data(data, data_TP53_in, 'idats')

############################################
###TERT 
############################################
#TERT data prep
column = 'TERT'
data_TERT_in = meta.dropna(subset =[column], axis = 0)
data_TERT_in = data_TERT_in[['idats',column]]
data_TERT_in.rename(columns = {'TERT':'TERT_Class'}, inplace = True)
column = 'TERT_Class'
TERT_classes = data_TERT_in.groupby(column).count().iloc[:,0] #count number for each group before sampling
data_TERT_in.to_excel('TERT_LABEL.xlsx', index = False)
data_TERT = prep_data(data, data_TERT_in, 'idats')

############################################
###SOME PLOTS
############################################
classes = pd.concat([os_classes,m_classes,v11_classes,TERT_classes,TP53_classes], axis=1, keys = ['os','m','v11','TERT','TP53'])
classes['index'] = pd.Series(classes.index, dtype = 'int')

classes_dist_plot(classes, 'classes.png')

#####     v11     os      m   TERT   TP53
# 0.0   597.0  367.0  601.0  411.0  490.0
# 1.0   997.0  251.0  368.0   87.0   38.0
# 2.0   420.0    NaN    NaN    NaN    NaN
# 3.0   344.0    NaN    NaN    NaN    NaN
# 4.0   234.0    NaN    NaN    NaN    NaN
# sum  2592.0  618.0  969.0  498.0  528.0

[['negative','positive'],['negative','positive'],['MB, G3','MB, G4','MB, SHH CHL AD','MB, SHH INF','MB, WNT'],['negative','positive'],['negative', 'positive']]

# v11
# MB, G3            597
# MB, G4            997
# MB, SHH CHL AD    420
# MB, SHH INF       344
# MB, WNT           234

### MAKE CN PLOTS FOR 3.1 and 3.2

cn_plot(data=cn_northcott.iloc[:,1:],cutoff=0.4,filename='amp_northcott.png')
cn_plot(data=cn_northcott.iloc[:,1:],cutoff=-0.4,filename='del_northcott.png')

### Nested CV + GRIDSEARCH RF ###

from ml_methods import *
import itertools
from joblib import Parallel, delayed

### Nested CV + GRIDSEARCH RF ###
rf_param_grid = {'rf__max_features': ['sqrt'] + [2**i for i in range(0,6)], 'rf__n_estimators': [1000]}
svm_param_grid = {'svm__kernel': ['rbf'], 'svm__gamma': ['auto', 10**-4, 10**-3, 10**-2, 10**-1, 10**0, 10**1]}
knn_param_grid = {'knn__n_neighbors': list(range(1,21))}
lr_param_grid = {}
seed = 1234

### COMBINE PARAMETER TO LOOP OVER THEM FOR EACH MODEL
methods = list(itertools.chain(*[[rf_param_grid]*6,[svm_param_grid]*6,[lr_param_grid]*6,[knn_param_grid]*6]))
pca_in = list(itertools.chain(*[[pca]*3,[None]*3]*4)) 
mv = list(itertools.chain(*[[None]*3,[True]*3]*4))
tag = list(itertools.chain(*[['RF']*6,['SVM']*6,['LR']*6,['knn']*6]))
model = ['Model'+str(i) for i in range(1,7)]*4
params = [i.tolist() for i in pd.DataFrame([methods,pca_in,mv,tag,model]).transpose().values]
p = 10


### RUN METHODS ON OS DATA
sampling = None
dataset = 'OS'
rf_param = list(zip(data_os,params[0:6]))
svm_param = list(zip(data_os,params[6:12]))
lr_param = list(zip(data_os,params[12:18]))
knn_param = list(zip(data_os,params[18:24]))

# OS WORKFLOW FOR RF
rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

# OS WORKFLOW FOR SVM
svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

# OS WORKFLOW FOR LR
lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

# OS WORKFLOW FOR KNN
knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)


### REPEAT THE SAME FOR DIFFERENT DATA

### RUN METHODS ON METASTASIS DATA

sampling = None
dataset = 'M'
rf_param = list(zip(data_M,params[0:6]))
svm_param = list(zip(data_M,params[6:12]))
lr_param = list(zip(data_M,params[12:18]))
knn_param = list(zip(data_M,params[18:24]))
p = 10

# M WORKFLOW FOR RF
rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

# M WORKFLOW FOR SVM
svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

# M WORKFLOW FOR LR
lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

# M WORKFLOW FOR KNN
knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)


### RUN METHODS ON METHYLATIONGROUP DATA

sampling = None
dataset = 'v11'
rf_param = list(zip(data_v11,params[0:6]))
svm_param = list(zip(data_v11,params[6:12]))
lr_param = list(zip(data_v11,params[12:18]))
knn_param = list(zip(data_v11,params[18:24]))

p = 5 # different p because more data -> more resources needed

# v11 WORKFLOW FOR RF
rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

# v11 WORKFLOW FOR SVM
svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

# v11 WORKFLOW FOR LR
lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

# v11 WORKFLOW FOR KNN
knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)


### RUN METHODS ON TP53 DATA

sampling = None
dataset = 'TP53'
rf_param = list(zip(data_TP53,params[0:6]))
svm_param = list(zip(data_TP53,params[6:12]))
lr_param = list(zip(data_TP53,params[12:18]))
knn_param = list(zip(data_TP53,params[18:24]))
p = 10

rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)


### RUN METHODS ON TP53 DATA WITH SMOTE OVERSAMPLING

sampling = 'SMOTE'
dataset = 'TP53_over'
rf_param = list(zip(data_TP53,params[0:6]))
svm_param = list(zip(data_TP53,params[6:12]))
lr_param = list(zip(data_TP53,params[12:18]))
knn_param = list(zip(data_TP53,params[18:24]))
p = 10

rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)

### RUN METHODS ON TP53 DATA WITH RANDOM UNDERSAMPLING

sampling = 'under'
dataset = 'TP53_under'
rf_param = list(zip(data_TP53,params[0:6]))
svm_param = list(zip(data_TP53,params[6:12]))
lr_param = list(zip(data_TP53,params[12:18]))
knn_param = list(zip(data_TP53,params[18:24]))
p = 10

rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)

### RUN METHODS ON TERT DATA

sampling = None
dataset = 'TERT'
rf_param = list(zip(data_TERT,params[0:6]))
svm_param = list(zip(data_TERT,params[6:12]))
lr_param = list(zip(data_TERT,params[12:18]))
knn_param = list(zip(data_TERT,params[18:24]))
p = 10

rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)

### RUN METHODS ON TERT DATA WITH SMOTE OVERSAMPLING

sampling = 'SMOTE'
dataset = 'TERT_over'
rf_param = list(zip(data_TERT,params[0:6]))
svm_param = list(zip(data_TERT,params[6:12]))
lr_param = list(zip(data_TERT,params[12:18]))
knn_param = list(zip(data_TERT,params[18:24]))
p = 10

rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)

### RUN METHODS ON TERT DATA WITH UNDERSAMPLING

sampling = 'under'
dataset = 'TERT_under'
rf_param = list(zip(data_TERT,params[0:6]))
svm_param = list(zip(data_TERT,params[6:12]))
lr_param = list(zip(data_TERT,params[12:18]))
knn_param = list(zip(data_TERT,params[18:24]))
p = 10

rf = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in rf_param]
print(' DONE RF for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_rf_results.sav'
joblib.dump(rf, filename)

svm = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in svm_param]
print(' DONE SVM for '+dataset.center(80, '*'))
filename = '../'+dataset+'_Models_svm_results.sav'
joblib.dump(svm, filename)

lr = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in lr_param]
print(' DONE LR '.center(80, '*'))
filename = '../'+dataset+'_Models_lr_results.sav'
joblib.dump(lr, filename)

knn = [use_pipeline(d, cv_inner, cv_outer, sampling, i[1], i[2], i[0], i[3], seed, dataset, i[4], p) for d,i in knn_param]
print(' DONE KNN '.center(80, '*'))
filename = '../'+dataset+'_Models_knn_results.sav'
joblib.dump(knn, filename)


