###IMPORTS
import os
from importlib import reload

import joblib
import numpy as np
import pandas as pd
from ml_methods import (prep_data, get_metrics,
                        nested_cv, sanity_check)

from visualize_results import *

###LOAD IN RESULTS

scores_os = joblib.load('../OS_MODELS/OS_scores.sav')
scores_v11 = joblib.load('../v11_MODELS/v11_scores.sav')
scores_M = joblib.load('../M_MODELS/M_scores.sav')
scores_TERT = joblib.load('../TERT_MODELS/TERT_scores.sav')
scores_TERT_over = joblib.load('../TERT_MODELS/TERT_over_scores.sav')
scores_TERT_under = joblib.load('../TERT_MODELS/TERT_under_scores.sav')
scores_TP53 = joblib.load('../TP53_MODELS/TP53_scores.sav')
scores_TP53_over = joblib.load('../TP53_MODELS/TP53_over_scores.sav')
scores_TP53_under = joblib.load('../TP53_MODELS/TP53_under_scores.sav')

###LOAD IN METADATA

label_os = pd.read_excel('OS_LABEL.xlsx')
label_m = pd.read_excel('M_LABEL.xlsx')
label_v11 = pd.read_excel('v11_LABEL.xlsx')
label_TERT = pd.read_excel('TERT_LABEL.xlsx')
label_TP53 = pd.read_excel('TP53_LABEL.xlsx')


# CLASSDISTRIBUTIONS
classes = pd.concat([label_os.groupby(label_os.columns[1]).count().iloc[:,0],
    label_m.groupby(label_m.columns[1]).count().iloc[:,0],
    label_v11.groupby(label_v11.columns[1]).count().iloc[:,0],
    label_TERT.groupby(label_TERT.columns[1]).count().iloc[:,0],
    label_TP53.groupby(label_TP53.columns[1]).count().iloc[:,0]], 
    axis=1, keys = ['os','m','v11','TERT','TP53'])
classes['index'] = pd.Series(classes.index, dtype = 'int')

classes_dist_plot(classes,'vert.png')

###CREATE PLOTS
#FIND FUNCTIONS IN VISUALIZE_RESULTS.PY

pred_boxplot(scores_os, 'class_os_boxplot.png')
pred_boxplot(scores_v11, 'class_v11_boxplot.png')
pred_boxplot(scores_M, 'class_M_boxplot.png')
pred_boxplot(scores_TERT, 'class_TERT_boxplot.png')
pred_boxplot(scores_TERT_over, 'class_TERT_over_boxplot.png', sampling = True)
pred_boxplot(scores_TERT_under, 'class_TERT_under_boxplot.png', sampling = True)
pred_boxplot(scores_TP53, 'class_TP53_boxplot.png')
pred_boxplot(scores_TP53_over, 'class_TP53_over_boxplot.png', sampling = True)
pred_boxplot(scores_TP53_under, 'class_TP53_under_boxplot.png', sampling = True)

sampling_plot(scores_TERT + scores_TERT_over + scores_TERT_under, 'sampling_TERT.png')
sampling_plot(scores_TP53 + scores_TP53_over + scores_TP53_under, 'sampling_TP53.png')

###CREATE TABLES

out_os = scores_eval(scores_os,'scores_os.xlsx')
out_m = scores_eval(scores_M,'scores_M.xlsx')
out_v11 = scores_eval(scores_v11,'scores_v11.xlsx')
out_tert = scores_eval(scores_TERT)
out_tert_over=scores_eval(scores_TERT_over, sampling = 'over')
out_tert_under=scores_eval(scores_TERT_under, sampling = 'under')
save_xls({**out_tert,**out_tert_over,**out_tert_under}, 'scores_TERT.xlsx')
out_tp53 = scores_eval(scores_TP53)
out_tp53_over=scores_eval(scores_TP53_over, sampling = 'over')
out_tp53_under=scores_eval(scores_TP53_under, sampling = 'under')
save_xls({**out_tp53,**out_tp53_over,**out_tp53_under}, 'scores_TP53.xlsx')

###CALCULATE ADDITIONAL METRICS; F1, PRECISION, RECALL

metrics_os = get_metrics(scores_os, label_os, 'os_metrics.xlsx')
metrics_M = get_metrics(scores_M, label_m, 'M_metrics.xlsx')
metrics_v11 = get_metrics(scores_v11, label_v11, 'v11_metrics.xlsx')
metrics_TERT = get_metrics(scores_TERT, label_TERT)
metrics_TERT_over = get_metrics(scores_TERT_over, label_TERT)
metrics_TERT_under = get_metrics(scores_TERT_under, label_TERT)
save_xls({**metrics_TERT,**metrics_TERT_over,**metrics_TERT_under}, 'TERT_metrics.xlsx')
metrics_TP53 = get_metrics(scores_TP53, label_TP53)
metrics_TP53_over = get_metrics(scores_TP53_over, label_TP53)
metrics_TP53_under = get_metrics(scores_TP53_under, label_TP53)
save_xls({**metrics_TP53,**metrics_TP53_over,**metrics_TP53_under}, 'TP53_metrics.xlsx')


########## combine and save by model and by class ##########

model1 = [j for i in results for j in i if 'Model1' in j['Set']]
model2 = [j for i in results for j in i if 'Model2' in j['Set']]
model3 = [j for i in results for j in i if 'Model3' in j['Set']]
model4 = [j for i in results for j in i if 'Model4' in j['Set']]
model5 = [j for i in results for j in i if 'Model5' in j['Set']]
model6 = [j for i in results for j in i if 'Model6' in j['Set']]

joblib.dump(model1, '../model1.sav')
joblib.dump(model2, '../model2.sav')
joblib.dump(model3, '../model3.sav')
joblib.dump(model4, '../model4.sav')
joblib.dump(model5, '../model5.sav')
joblib.dump(model6, '../model6.sav')

class_os = [j for i in results for j in i if 'osstatus' in j['Set']]
class_m = [j for i in results for j in i if 'M_' in j['Set']]
class_v11 = [j for i in results for j in i if 'v11' in j['Set']]
class_tert = [j for i in results for j in i if 'TERT' in j['Set']]
class_tp53 = [j for i in results for j in i if 'TP53' in j['Set']]

joblib.dump(class_os, '../class_os.sav')
joblib.dump(class_m, '../class_m.sav')
joblib.dump(class_v11, '../class_v11.sav')
joblib.dump(class_tert, '../class_tert.sav')
joblib.dump(class_tp53, '../class_tp53.sav')

