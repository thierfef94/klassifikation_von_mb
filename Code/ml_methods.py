### IMPORTS

import matplotlib.pyplot as plt
from sklearn import metrics
import seaborn as sn
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plot
from joblib import Parallel, delayed
import imblearn
from sklearn.model_selection import (GridSearchCV, ParameterGrid,
                                    StratifiedKFold, KFold, cross_val_score)
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn import svm																	
from sklearn.neighbors import KNeighborsClassifier

from imblearn.over_sampling import SMOTE
#from sklearn.pipeline import Pipeline
from imblearn.pipeline import Pipeline
from imblearn.under_sampling import RandomUnderSampler
from sklearn.model_selection import cross_val_score, GridSearchCV, cross_validate

from sklearn.base import BaseEstimator
import numpy as np
from sklearn.utils.validation import check_is_fitted

from sklearn.model_selection import cross_val_predict

### DEFINE DIMENSIONS FOR PCA AND CALCULATION OF MOST VARIABLE 
NUM_GENES = 43
NUM_GENES_FULL = 21211

### CALCULATIONS OF THE METRICS

def get_metrics(testLabels, predictions, label):
	# Model Accuracy: how often is the classifier correct?
	acc = metrics.accuracy_score(testLabels, predictions)
	#print("%s Accuracy: %.2f" % (label, acc))
	# Model Balanced Accuracy: how often is the classifier correct?
	bacc = metrics.balanced_accuracy_score(testLabels, predictions)
	#print("%s Balanced Accuracy: %.2f" % (label, bacc))
	# Confusion Matrix
	if(len(set(testLabels)) == 2): # for binary models
		conf_matrix_cols = ['1', '0']
		conf_matrix = metrics.confusion_matrix(testLabels, predictions)
		TP = conf_matrix[1,1]
		TN = conf_matrix[0,0]
		FP = conf_matrix[0,1]
		FN = conf_matrix[1,0]
		conf_matrix_df = pd.DataFrame(conf_matrix, index = conf_matrix_cols, columns = conf_matrix_cols)
		# ROC Curve
		fpr,tpr,_ = metrics.roc_curve(testLabels, predictions)
		auc = metrics.roc_auc_score(testLabels, predictions)
		# Model Precision: what percentage of positive tuples are labeled as such?
		prec = metrics.precision_score(testLabels, predictions)
		recall = metrics.recall_score(testLabels, predictions)
		f1 = metrics.f1_score(testLabels, predictions)
		mean_absolute_err = metrics.mean_absolute_error(testLabels, predictions)
		# print('%s Mean Absolute Error: %.2f' % (label, mean_absolute_err))
		# Mean Squared Error
		mean_squared_err = metrics.mean_squared_error(testLabels, predictions)
		# print('%s Mean Squared Error: %.2f' % (label, mean_squared_err))
		all_metrics = pd.DataFrame({label :[acc, bacc,TP, TN, FP, FN, auc, prec, recall, f1, mean_absolute_err, mean_squared_err]}, index = ['Accuracy','Balanced Accuracy','TP' , 'TN', 'FP', 'FN', 'AUC Score', 'Precision', 'Recall', 'F1 Score', 'Mean Absolute Error', 'Mean Squared Error'])
	else: # for multiclass models
		# auc = metrics.roc_auc_score(testLabels, predictions, average = 'micro', multi_class = 'ovo')
		prec = metrics.precision_score(testLabels, predictions, average='micro')
		recall = metrics.recall_score(testLabels, predictions, average ='micro')
		f1 = metrics.f1_score(testLabels, predictions, average = 'micro')
		mean_absolute_err = metrics.mean_absolute_error(testLabels, predictions)
		# print('%s Mean Absolute Error: %.2f' % (label, mean_absolute_err))
		# Mean Squared Error
		mean_squared_err = metrics.mean_squared_error(testLabels, predictions)
		# print('%s Mean Squared Error: %.2f' % (label, mean_squared_err))
		all_metrics = pd.DataFrame({label :[acc, bacc, prec, recall, f1, mean_absolute_err, mean_squared_err]}, index = ['Accuracy','Balanced Accuracy', 'Precision', 'Recall', 'F1 Score', 'Mean Absolute Error', 'Mean Squared Error'])
	return(all_metrics)	

def get_metrics_cv(data, cv, model, label, do_pca, pca):
    if (do_pca):
        data.reset_index(inplace = True, drop = True)
        beta_PCA = pd.DataFrame(pca.fit_transform(data.iloc[:,59:]))
        data = pd.concat([data.iloc[:,:59],beta_PCA], axis = 1)
        #print(data)
    res = []
    for _, test in cv.split(data):
        test_X = data.iloc[test,1:]
        test_y = data.iloc[test,0]
        prediction = model.predict(test_X)
        res.append(get_metrics(test_y,prediction,label))
    scores = pd.concat(res, axis = 1)
    scores.columns = range(0, len(res))
    scores_mean = pd.DataFrame(scores.mean(axis = 1), columns = ['mean'])
    return (scores, scores_mean)

def get_metrics_evaluation(results):
	label = [i['Set'] for i in results]
	testLabels = [i['CV-FOLD Lables'] for i in results]
	predictions = [i['CV-Fold Model Predictions'] for i in results]

	scores_per_model = pd.concat([get_metrics(j,k,label[i]) for i in range(0,len(label)) for j,k in zip(testLabels[i], predictions[i])], axis = 1)
	scores_per_model.columns = label
	# scores_mean = scores_per_model.mean(axis = 1)
	# scores = pd.concat([scores_per_model,scores_mean], axis=1)
	# scores.columns = label+['mean']
	scores = scores_per_model
	return(scores)


### PREPARE DATA
def prep_data(data, meta, merge_col):
### INPUT DATA DATAFRAME
### INPUT METADATA DATAFRAME
### INPUT NAME OF COLUMN TO MERGE METADATA AND FEATURE DATA TABLES
### OUTPUT LIST OF FEATURE DATA DATAFRAMES FOR ALL NON NA METADATA ROWS FOR EACH OF THE SIX MODELS
	model1 = meta.merge(data[2], on = merge_col) #Model 1: Klassifikation auf 43 Northcott Genes CNV
	model1.drop(merge_col, axis = 1, inplace = True)
	model2 = meta.merge(data[0], on = merge_col) #Model 2: Klassifikation auf 43 PCAs beta
	model2.drop(merge_col, axis = 1, inplace = True)
	model3 = meta.merge(data[2].merge(data[0], on = merge_col), on = merge_col) #Model 3: 43 Northcott Genes CNV + 43 PCAs beta
	model3.drop(merge_col, axis = 1, inplace = True)
	model4 = meta.merge(data[1],on = merge_col) #Model 4: 10000 Gene CNVs most variable
	model4.drop(merge_col, axis = 1, inplace = True)
	model5 = meta.merge(data[0], on = merge_col) #Model 2: Klassifikation auf 43 PCAs beta
	model5.drop(merge_col, axis = 1, inplace = True)
	model6 = meta.merge(data[1].merge(data[0], on = merge_col), on = merge_col) #Model 6: 10000 Gene CNVs most variable + 10000 most variable CPgs
	model6.drop(merge_col, axis = 1, inplace = True)

	return([model1, model2, model3, model4, model5, model6])

def logistic_model(train_outer_X, train_outer_y, test_outer_X, test_outer_y, pca, seed, n_jobs):
	model_grid = LogisticRegression(penalty= 'none', solver='saga', multi_class= 'ovr', max_iter = 10000, n_jobs = n_jobs, random_state= seed)
	best_model = model_grid.fit(train_outer_X, train_outer_y)
	return(best_model)

def inner_cv(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs = 5):
	best_result = -1
	best_params = []
	for parameter in grid_param:
		inner_results = []
		dim_reduction_inner = []
		for train_index_inner, test_index_inner in cv_inner.split(train_outer_X, train_outer_y):
			#print(train_outer_y.value_counts().tolist())
			train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X.iloc[train_index_inner,:], train_outer_y.iloc[train_index_inner], sampling, seed)
			#print(train_outer_y_sampled.value_counts().tolist())
			if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, train_outer_X.iloc[test_index_inner,:], pca) # only do pca
			else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, train_outer_X.iloc[test_index_inner,:], mv)
			train_inner_X, test_inner_X = combine_train, combine_test
			train_inner_y, test_inner_y = train_outer_y_sampled, train_outer_y.iloc[test_index_inner]
			if (method == 'rf'):
				model_grid = RandomForestClassifier(n_estimators = parameter['n_estimators'], random_state= seed, max_features = parameter['max_features'], n_jobs = n_jobs)
			if (method == 'svm'):
				model_grid = svm.SVC(kernel = parameter['kernel'], gamma = parameter['gamma'], random_state= seed)
			if (method == 'knn'):
				model_grid = KNeighborsClassifier(n_neighbors = parameter['n_neighbors'], n_jobs = n_jobs)
			model_grid.fit(train_inner_X, train_inner_y)
			model_predict = model_grid.predict(test_inner_X)
			bacc_grid = metrics.balanced_accuracy_score(test_inner_y ,model_predict)
			inner_results.append(bacc_grid)
			dim_reduction_inner.append(dim_reduction)
		inner_results_mean = np.array(inner_results).mean()
		if (inner_results_mean > best_result):
			best_params = parameter
			best_dim_reduction = dim_reduction_inner.pop()
			best_result = inner_results_mean
	return(best_params, best_dim_reduction)


def nested_cv(data, cv_outer, cv_inner, grid_param, pca, mv, method, seed, dataset, model, sampling, n_jobs = 5):
	# enumerate splits
	print("starting nested CV for "+dataset+'_'+method+'_'+model)
	method = method.lower()
	results = [] # variable to save results for evaluation of nested cv
	best_result = -1 # variable to choose best model
	best_parameter = [] # variable to save parameter of best model
	model_test_y = [] # variable to save correct classes for evaluation of nested cv
	model_predictions = [] # variable to save predicted classes for evaluation of nested cv
	data.reset_index(inplace = True, drop = True) # reset index to be able to concatenate when doing pca with combined input data (betas and cn values)
	for train_index_outer, test_index_outer  in cv_outer.split(data.iloc[:,1:], data.iloc[:,0]): # outer cv of nested cv
		# split data with cv outer split indeces
		train_outer_X, test_outer_X = data.iloc[train_index_outer, 1:], data.iloc[test_index_outer, 1:]
		train_outer_y, test_outer_y = data.iloc[train_index_outer,0], data.iloc[test_index_outer,0]
		# define the model
		if (method == 'rf'): # Random Forest Classifier
			best_param, dim_reduction = inner_cv(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs) # inner cv with grid search
			model_outer = RandomForestClassifier(n_estimators = 1000, random_state= seed, max_features = best_param['max_features'], n_jobs = n_jobs) # define outer cv model with best parameters from grid search
		if (method == 'svm'): # SVM Model
			best_param, dim_reduction = inner_cv(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs) # inner cv with grid search
			model_outer = svm.SVC(kernel = best_param['kernel'], gamma = best_param['gamma'], random_state= seed) # define outer cv model with best parameters from grid search
		if ((method == 'logistic') | (method == 'lr')): # Logistic Regression no inner cv needed, because we don't have to do grid search
			train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X, train_outer_y, sampling, seed)
			if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, test_outer_X, pca) # only do pca
			else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, test_outer_X, mv)
			model_outer = logistic_model(combine_train, train_outer_y_sampled, combine_test, test_outer_y, pca, seed, n_jobs) # do model definition + fit
			best_param = grid_param # best parameter are input parameter
		if (method == 'sanity_rf'): # Random Forest Classifier
			train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X, train_outer_y, sampling, seed)
			if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, test_outer_X, pca) # only do pca
			else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, test_outer_X, mv)
			model_outer = RandomForestClassifier(random_state=seed, n_jobs = n_jobs) # define outer cv model with best parameters from grid search
			best_param = grid_param # best parameter are input parameter
		if (method == 'sanity_svm'): # SVM Model
			train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X, train_outer_y, sampling, seed)
			if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, test_outer_X, pca) # only do pca
			else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, test_outer_X, mv)
			model_outer = svm.SVC(random_state= seed) # define outer cv model with best parameters from grid search
			best_param = grid_param # best parameter are input parameter
		if (method == 'knn'):
			best_param, dim_reduction = inner_cv(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs) # inner cv with grid search
			model_outer = KNeighborsClassifier(n_neighbors = best_param['n_neighbors'], n_jobs = n_jobs)
		model_outer_predict = outer_cv(model_outer, train_outer_X, test_outer_X, train_outer_y, dim_reduction, mv)
		model_test_y.append(test_outer_y)
		model_predictions.append(model_outer_predict)
		outer_bacc = metrics.balanced_accuracy_score(test_outer_y, model_outer_predict)
		results.append(outer_bacc)
		print('Outer Result BACC: %.2f%%' %(outer_bacc))
		if (outer_bacc > best_result):
			best_result = outer_bacc
			best_parameter = best_param
			best_model_final = model_outer
			print('new best outer parameters ', best_parameter)
			print('with score of %.2f%%' %(outer_bacc*100))
	print('nested CV for Set '+dataset+'_'+method+'_'+model+' done!')
	return({'Set':dataset+'_'+method+'_'+model,'best_model': best_model_final, 'best_parameter': best_parameter, 'best_score': best_result, 'scores': results, 'CV-FOLD Lables': model_test_y, 'CV-Fold Model Predictions': model_predictions})

def get_plots(results, label):
	# filename = '/mnt/scratch2/felix/data/'+label.upper()+'_Models_results.sav'
	# results = joblib.load(filename)
	res_rf_combine = results['rf_combine_'+label]
	res_rf_beta = results['rf_beta_'+label]
	res_rf_cn = results['rf_cn_'+label]
	res_svm_combine = results['svm_combine_'+label]
	res_svm_beta = results['svm_beta_'+label]
	res_svm_cn = results['svm_cn_'+label]
	res_logistic_combine = results['logistic_combine_'+label]
	res_logistic_beta = results['logistic_beta_'+label]
	res_logistic_cn = results['logistic_cn_'+label]

	fig1, ax1 = plot.subplots()
	fig1.subplots_adjust(bottom=0.3)
	ax1.set_title(label)
	ax1.xaxis.set_ticklabels(['RF_Combined','RF_betas','RF_CN', 'SVM_Combined','SVM_betas','SVM_CN', 'LogReg_Combined','LogReg_betas','LogReg_CN'], rotation=90)
	ax1.set_ylim(0.4,1.1)
	boxplot = ax1.boxplot([res_rf_combine['scores'],res_rf_beta['scores'], res_rf_cn['scores'],res_svm_combine['scores'],res_svm_beta['scores'], res_svm_cn['scores'], res_logistic_combine['scores'],res_logistic_beta['scores'], res_logistic_cn['scores']], patch_artist = True)
    # fill with colors
	colors = ['purple', 'blue', 'red']
	for patch, color in zip(boxplot['boxes'], colors*3):
		patch.set_facecolor(color)

	fig1.savefig('/mnt/scratch2/felix/plots/ML/'+label+'_boxplot.png')
	plot.close()

	scores = get_metrics_evaluation(results, label)

	for score, ml in zip(scores,['RF','SVM','LR']):
		score.to_csv('scores_'+ml+'_'+label+'.csv')

		plot_scores = pd.concat([score.combination['mean'], score.beta['mean'], score.cn['mean']], keys = ['combination', 'beta', 'cn'], axis = 1)
		try: 
			plot_scores.drop(labels = ['TN','TP','FN', 'FP'], inplace = True)
		except KeyError:
			pass

		fig_scores, ax_scores = plot.subplots()
		fig_scores.subplots_adjust(bottom=0.4)
		# set width of bar
		barWidth = 0.25
        
		# set height of bar
		bars1 = plot_scores.combination.values.tolist()
		bars2 = plot_scores.beta.values.tolist()
		bars3 = plot_scores.cn.values.tolist()
        
        # Set position of bar on X axis
		r1 = np.arange(len(bars1))
		r2 = [x + barWidth for x in r1]
		r3 = [x + barWidth for x in r2]
        
		# Make the plot
		ax_scores.bar(r1, bars1, color='purple', width=barWidth, edgecolor='white', label='Combination')
		ax_scores.bar(r2, bars2, color='blue', width=barWidth, edgecolor='white', label='beta only')
		ax_scores.bar(r3, bars3, color='red', width=barWidth, edgecolor='white', label='CN only')
        
		# Add xticks on the middle of the group bars
		ax_scores.set_title(label.upper()+' - Scores '+ml)
		ax_scores.set_ylim(-0.05,1.1)
		plot.xlabel('Score', fontweight='bold')
		plot.xticks([r + barWidth for r in range(len(bars1))], plot_scores.index.values.tolist(), rotation = 90)
		plot.legend()

		fig_scores.savefig('/mnt/scratch2/felix/plots/ML/scores_'+ml+'_'+label+'.png')
		plot.close()


def outer_cv(model_outer, train_outer_X, test_outer_X, train_outer_y, dim_reduction, mv):
		if (mv == False): combine_train, combine_test, _ = do_pca(train_outer_X, test_outer_X, dim_reduction)
		else: combine_train, combine_test = train_outer_X.loc[:,dim_reduction], test_outer_X.loc[:,dim_reduction]
		model_outer.fit(combine_train, train_outer_y)
		return model_outer.predict(combine_test) 

def do_pca(data_train, data_test, pca):
	fit = True # True or False wether we have an already fitted set from inner CV
	try: # try if n_features exists, if it does, its fitted, if not we need to fit to the input training set
		#print('try')
		pca.n_features_
	except AttributeError: # error means its not fitted
		#print('not fitted')
		fit = False
		pass
	if(fit) : pca_fit = pca # set function intern PCA Variable to input PCA if input already fitted
	if(data_train.shape[1] == NUM_GENES): # Model 1: Klassifiaktion auf 43 Northcott Genes CNV
		pca_fit = 'Northcott CN only' # cause PCA needed
		combine_train = data_train # no PCA Needed hence only pipeping it to the output
		combine_test = data_test
	if(data_train.shape[1] == 409800): # Model 2 and 5: Klassifikation auf 43 PCAs beta, oder einfacher 43 most variable betas and 10000 most variable CPgs
		if (not(fit)): pca_fit = pca.fit(data_train.iloc[:,:]) # if not fitted do fit on trainings set
		train_transform = pd.DataFrame(pca_fit.transform(data_train.iloc[:,:])) # transform train data
		combine_train = train_transform # combine to one DataFrame
		test_transform = pd.DataFrame(pca_fit.transform(data_test.iloc[:,:])) # transform test data
		combine_test = test_transform
	if(data_train.shape[1] == 409843): # Model 3: 43 Northcott Genes CNV + 43 PCAs beta
		if (not(fit)): pca_fit = pca.fit(data_train.iloc[:,NUM_GENES:]) # if not fitted do fit on trainings set
		train_transform_cn = data_train.iloc[:,:NUM_GENES].reset_index(drop=True) # transform train data
		train_transform_beta = pd.DataFrame(pca_fit.transform(data_train.iloc[:,NUM_GENES:])) # transform train data
		combine_train = pd.concat([train_transform_cn, train_transform_beta], axis = 1) # combine to one DataFrame
		test_transform_cn = data_test.iloc[:,:NUM_GENES].reset_index(drop=True) # transform test data
		test_transform_beta = pd.DataFrame(pca_fit.transform(data_test.iloc[:,NUM_GENES:])) # transform test data
		combine_test = pd.concat([test_transform_cn, test_transform_beta], axis = 1)
	return([combine_train, combine_test, pca_fit]) # Return the pca'd train and test set + the PCA variable to use it later

def n_most_variable(data, n, axis = 0):
	names = data.std(axis = axis).sort_values(ascending = False).index[:n].tolist()
	if(axis == 0): return [data.loc[:,names],names]
	if(axis == 1): return [data.loc[names,:],names]

def get_most_variable(data_train, data_test, n):
	if(data_train.shape[1] == 409800): # Model 5: 10000 most variable CPgs
		out_train, names = n_most_variable(data_train.iloc[:,:],n) # get n most variable cpgs as data frame and also their names
		out_test = data_test.loc[:,names] # use names to filter test set 
	if(data_train.shape[1] == 21211): # Model 4: 10000 Gene CNVs most variable
		out_train, names = n_most_variable(data_train.iloc[:,:],n) # get n most variable CN as data frame and also their names
		out_test = data_test.loc[:,names] # use names to filter test set 
	if(data_train.shape[1] == 431011): # Model 6: 10000 Gene CNVs most variable + 10000 most variable CPgs
		out_train_cn, names_cn = n_most_variable(data_train.iloc[:,:NUM_GENES_FULL],n) # get n most variable CN as data frame and also their names
		out_test_cn = data_test.loc[:,names_cn] # use names to filter test set 
		out_train_beta, names_beta = n_most_variable(data_train.iloc[:,NUM_GENES_FULL:],n) # get n most variable cpgs as data frame and also their names
		out_test_beta = data_test.loc[:,names_beta] # use names to filter test set 
		out_train = pd.concat([out_train_cn, out_train_beta], axis = 1) # combine them all to a new combined trainings data set
		out_test = pd.concat([out_test_cn, out_test_beta], axis = 1) # for a new test data set aswell
		names = names_cn+names_beta # save their names
	return([out_train, out_test, names]) # return new data sets and column names

def mc_inner(train_outer_X, train_outer_y, pca, mv, method, seed, n_jobs, parameter, train_index_inner, test_index_inner, sampling):
	train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X.iloc[train_index_inner,:], train_outer_y.iloc[train_index_inner], sampling, seed)
	if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, train_outer_X.iloc[test_index_inner,:], pca) # only do pca
	else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, train_outer_X.iloc[test_index_inner,:], mv)
	train_inner_X, test_inner_X = combine_train, combine_test
	train_inner_y, test_inner_y = train_outer_y_sampled, train_outer_y.iloc[test_index_inner]
	if (method == 'rf'):
		model_grid = RandomForestClassifier(n_estimators = parameter['n_estimators'], random_state= seed, max_features = parameter['max_features'], n_jobs = n_jobs)
	if (method == 'svm'):
		model_grid = svm.SVC(kernel = parameter['kernel'], gamma = parameter['gamma'], random_state= seed)
	if (method == 'knn'):
		model_grid = KNeighborsClassifier(n_neighbors = parameter['n_neighbors'], n_jobs = n_jobs)
	
	fitted_model = model_grid.fit(train_inner_X, train_inner_y)
	return(fitted_model, test_inner_X, test_inner_y, dim_reduction)

def inner_cv_mc(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs = 5):
	best_result = -1
	best_params = []
	for parameter in grid_param:
		inner_results, dim_reduction_inner = [],[]
		inner_model_grid = Parallel(n_jobs = 10)(delayed(mc_inner)(train_outer_X, train_outer_y, pca, mv, method, seed, n_jobs, parameter, train_index_inner, test_index_inner, sampling) for  train_index_inner, test_index_inner in cv_inner.split(train_outer_X, train_outer_y))
		for model_grid in inner_model_grid:
			model_predict = model_grid[0].predict(model_grid[1])
			bacc_grid = metrics.balanced_accuracy_score(model_grid[2] ,model_predict)
			inner_results.append(bacc_grid)
			dim_reduction_inner.append(model_grid[3])
		inner_results_mean = np.array(inner_results).mean()
		if (inner_results_mean > best_result):
			best_params = parameter
			best_dim_reduction = dim_reduction_inner.pop()
			best_result = inner_results_mean
	return(best_params, best_dim_reduction)

def nested_cv_mc(data, cv_outer, cv_inner, grid_param, pca, mv, method, seed, dataset, model, sampling, n_jobs = 5):
	# enumerate splits
	print("starting nested CV for "+dataset+'_'+method+'_'+model)
	method = method.lower()
	results = [] # variable to save results for evaluation of nested cv
	best_result = -1 # variable to choose best model
	best_parameter = [] # variable to save parameter of best model
	model_test_y = [] # variable to save correct classes for evaluation of nested cv
	model_predictions = [] # variable to save predicted classes for evaluation of nested cv
	data.reset_index(inplace = True, drop = True) # reset index to be able to concatenate when doing pca with combined input data (betas and cn values)
	for train_index_outer, test_index_outer  in cv_outer.split(data.iloc[:,1:], data.iloc[:,0]): # outer cv of nested cv
		# split data with cv outer split indeces
		train_outer_X, test_outer_X = data.iloc[train_index_outer, 1:], data.iloc[test_index_outer, 1:]
		train_outer_y, test_outer_y = data.iloc[train_index_outer,0], data.iloc[test_index_outer,0]
		# define the model
		if (method == 'rf'): # Random Forest Classifier
			best_param, dim_reduction = inner_cv_mc(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs) # inner cv with grid search
			model_outer = RandomForestClassifier(n_estimators = 1000, random_state= seed, max_features = best_param['max_features'], n_jobs = n_jobs) # define outer cv model with best parameters from grid search
		if (method == 'svm'): # SVM Model
			best_param, dim_reduction = inner_cv_mc(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs) # inner cv with grid search
			model_outer = svm.SVC(kernel = best_param['kernel'], gamma = best_param['gamma'], random_state= seed) # define outer cv model with best parameters from grid search
		if ((method == 'logistic') | (method == 'lr')): # Logistic Regression no inner cv needed, because we don't have to do grid search
			train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X, train_outer_y, sampling, seed)
			if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, test_outer_X, pca) # only do pca
			else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, test_outer_X, mv)
			model_outer = logistic_model(combine_train, train_outer_y_sampled, combine_test, test_outer_y, pca, seed, n_jobs) # do model definition + fit
			best_param = grid_param # best parameter are input parameter
		if (method == 'sanity_rf'): # Random Forest Classifier
			train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X, train_outer_y, sampling, seed)
			if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, test_outer_X, pca) # only do pca
			else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, test_outer_X, mv)
			model_outer = RandomForestClassifier(random_state=seed, n_jobs = n_jobs) # define outer cv model with best parameters from grid search
			best_param = grid_param # best parameter are input parameter
		if (method == 'sanity_svm'): # SVM Model
			train_outer_X_sampled, train_outer_y_sampled = do_sampling(train_outer_X, train_outer_y, sampling, seed)
			if (mv == False): combine_train, combine_test, dim_reduction = do_pca(train_outer_X_sampled, test_outer_X, pca) # only do pca
			else: combine_train, combine_test, dim_reduction = get_most_variable(train_outer_X_sampled, test_outer_X, mv)
			model_outer = svm.SVC(random_state= seed) # define outer cv model with best parameters from grid search
			best_param = grid_param # best parameter are input parameter
		if (method == 'knn'):
			best_param, dim_reduction = inner_cv_mc(train_outer_X, train_outer_y, grid_param, cv_inner, pca, mv, method, seed, sampling, n_jobs) # inner cv with grid search
			model_outer = KNeighborsClassifier(n_neighbors = best_param['n_neighbors'], n_jobs = n_jobs)
		model_test_y.append(test_outer_y)
		model_outer_predict = outer_cv(model_outer, train_outer_X, test_outer_X, train_outer_y, dim_reduction, mv)
		model_predictions.append(model_outer_predict)
		outer_bacc = metrics.balanced_accuracy_score(test_outer_y, model_outer_predict)
		results.append(outer_bacc)
		print('Outer Result BACC: %.2f%%' %(outer_bacc))
		if (outer_bacc > best_result):
			best_result = outer_bacc
			best_parameter = best_param
			best_model_final = model_outer
			print('new best outer parameters ', best_parameter)
			print('with score of %.2f%%' %(outer_bacc*100))
	print('nested CV for Set '+dataset+'_'+method+'_'+model+' done!')
	return({'Set':dataset+'_'+method+'_'+model,'best_model': best_model_final, 'best_parameter': best_parameter, 'best_score': best_result, 'scores': results, 'CV-FOLD Lables': model_test_y, 'CV-Fold Model Predictions': model_predictions})

### WRAPPER FOR SAMPLING METHODS

def do_sampling(data_X, data_Y, sampling, seed):	
	counts = data_Y.value_counts().tolist()
	minority = counts[len(counts)-1]
	majority = counts[0]
	if(sampling == 'SMOTE'): ### SMOTE OVERSAMPLING
		over = imblearn.over_sampling.SMOTE()
		fit_over = over.fit_resample(data_X,data_Y)
		return fit_over
	if(sampling == 'randover'): ### RANDOMOVERSAMPLING ### NOT USED
		combine = pd.concat([data_Y,data_X], axis = 1)
		data = pd.concat([combine[data_Y.values == 0], combine[data_Y.values == 1].sample(majority, replace=True, random_state = seed)])
		data.reset_index(drop = True, inplace = True)
		return data.iloc[:,1:],data.iloc[:,0]
	elif (sampling == 'randunder'): ### RANDOM UNDERSAMPLING
		combine = pd.concat([data_Y,data_X], axis = 1)
		data = pd.concat([combine[data_Y.values == 1], combine[data_Y.values == 0].sample(minority, replace=True, random_state = seed)])
		data.reset_index(drop = True, inplace = True)
		return data.iloc[:,1:],data.iloc[:,0]
	elif (sampling == None):
		return data_X, data_Y

### RUN WITH DEFAULT PARAMETER

def sanity_check(data, data_beta, data_cn, cv_outer, cv_inner, pca, dataset, seed, n_jobs):
	print('RF')
	res_rf_combine = nested_cv(data, cv_outer, cv_inner, None,  pca, 'sanity_rf', seed,dataset,'Model'+str(i), n_jobs)
	res_rf_beta = nested_cv(data_beta, cv_outer, cv_inner, None,  pca, 'sanity_rf', seed,dataset,'Model'+str(i), n_jobs)
	res_rf_cn = nested_cv(data_cn, cv_outer, cv_inner, None,  pca, 'sanity_rf', seed,dataset,'Model'+str(i), n_jobs)
	print('SVM')
	res_svm_combine = nested_cv(data, cv_outer, cv_inner, None,  pca, 'sanity_svm', seed,dataset,'Model'+str(i), n_jobs)
	res_svm_beta = nested_cv(data_beta, cv_outer, cv_inner, None, pca, 'sanity_svm', seed,dataset,'Model'+str(i), n_jobs)
	res_svm_cn = nested_cv(data_cn, cv_outer, cv_inner, None, pca, 'sanity_svm', seed,dataset,'Model'+str(i), n_jobs)

	results = {'rf_combine_'+dataset: res_rf_combine, 'rf_beta_'+dataset: res_rf_beta, 'rf_cn_'+dataset: res_rf_cn, 'svm_combine_'+dataset: res_svm_combine, 'svm_beta_'+dataset: res_svm_beta, 'svm_cn_'+dataset: res_svm_cn, 'sanity_rf_combine_'+dataset: res_rf_combine, 'sanity_rf_beta_'+dataset: res_rf_beta, 'sanity_rf_cn_'+dataset: res_rf_cn, 'sanity_svm_combine_'+dataset: res_svm_combine, 'sanity_svm_beta_'+dataset: res_svm_beta, 'sanity_svm_cn_'+dataset: res_svm_cn}  
	return(results)

###CLASSES FOR PIPELINE


### CLASS TO CALCULATE MOST VARIABLE GENES

class most_variable(BaseEstimator):
	def __init__(self, n = 10000, axis = 0):
		self.n = n
		self.axis = axis
	def get_names(self):
		check_is_fitted(self)
		return self.names_
	
	def fit(self, X):
		self.X_ = X
		names = pd.DataFrame(X).std(axis = self.axis).sort_values(ascending = False).index[:self.n].tolist()
		self.names_ = names
		return self
	
	def transform(self, X):
		check_is_fitted(self)
		if (self.axis == 0): return X[:,self.names_]
		if (self.axis == 1): return X[self.names_,:]

	def fit_transform(self, X):
		fit = self.fit(X)
		return fit.transform(X)

### CLASS TO CALCULATE MOST VARIABLE GENES SPECIFICALLY FOR THE MODELS DIFFERENTIATED BY DIMENSIONS

class d_most_variable(BaseEstimator):
	NUM_GENES = 43
	NUM_GENES_FULL = 21211
	def __init__(self, n = 10000, axis = 0, NUM_GENES_FULL = NUM_GENES_FULL ):
		self.n = n
		self.axis = axis
	
	def get_names(self):
		check_is_fitted(self)
		return self.names_

	def fit(self, X, y = None):
		self.X_ = X
		self.y_ = y
		if((X.shape[1] == 409800) | (X.shape[1] == 21211)): # Model 5: 10000 most variable CPgs
			mv = most_variable()
			mv_fit = mv.fit_transform(X)
			self.names_ = mv.get_names()
		if(X.shape[1] == 431011): # Model 6: 10000 Gene CNVs most variable + 10000 most variable CPgs
			mv_cn = most_variable()
			mv_cn.fit(X[:,:NUM_GENES_FULL])
			names_cn = mv_cn.get_names()
			mv_beta = most_variable()
			mv_beta.fit(X[:,NUM_GENES_FULL:])
			names_beta = mv_beta.get_names()
			self.names_ = names_cn+names_beta # save their names
		return self
	
	def transform(self, X, y=None):
		check_is_fitted(self)
		if (self.axis == 0): return X[:,self.names_]
		if (self.axis == 1): return X[self.names_,:]

	def fit_transform(self, X, y = None):
		fit = self.fit(X)
		return fit.transform(X)

### CLASS TO DO PCA, BUT SPECIFICALLY FOR NEEDED DIMENSIONS

class d_pca(BaseEstimator):
	NUM_GENES = 43
	NUM_GENES_FULL = 21211
	def __init__(self, pca):
		self.pca = pca
		self.n_components = pca.n_components
		self.random_state = pca.random_state

	def fit(self, X, y=None):
		self.X_ = X
		self.y_ = y
		if(X.shape[1] == NUM_GENES): # Model 1: Klassifiaktion auf 43 Northcott Genes CNV
			#p.fit = 'Northcott CN only'
			self.pca.fit(X)
		if(X.shape[1] == 409800): # Model 2 and 5: Klassifikation auf 43 PCAs beta, oder einfacher 43 most variable betas and 10000 most variable CPgs
			self.pca.fit(X) # if not fitted do fit on trainings set
		if(X.shape[1] == 409843): # Model 3: 43 Northcott Genes CNV + 43 PCAs beta
			self.pca.fit(X[:,NUM_GENES:])
		return self
	
	def transform(self, X):
		check_is_fitted(self.pca)
		if(X.shape[1] == NUM_GENES): # Model 1: Klassifiaktion auf 43 Northcott Genes CNV
			self.data = X
		if(X.shape[1] == 409800): # Model 2 and 5: Klassifikation auf 43 PCAs beta, oder einfacher 43 most variable betas and 10000 most variable CPgs
			self.data = self.pca.transform(X) # if not fitted do fit on trainings set
		if(X.shape[1] == 409843): # Model 3: 43 Northcott Genes CNV + 43 PCAs beta
			train_transform_cn = X[:,:NUM_GENES]#.reset_index(drop=True) # transform train data
			train_transform_beta = self.pca.transform(X[:,NUM_GENES:]) # transform train data
			self.data = np.concatenate([train_transform_cn, train_transform_beta], axis = 1) # combine to one DataFrame
		return self.data

	def fit_transform_(self, X):
		fit = self.fit(X)
		return fit.transform(X)

def get_Xy(data):
    data_X = data.iloc[:,1:].to_numpy()
    data_y = data.iloc[:,:1].to_numpy().ravel()
    return data_X,data_y

def get_method(method, seed):
	if (method.lower() == 'knn'):
		method_func = KNeighborsClassifier(n_jobs = 1)
	elif (method.lower() == 'rf'): 
		method_func = RandomForestClassifier(random_state = seed, n_jobs = 1)
	elif (method.lower() == 'svm'):
		method_func = svm.SVC(random_state = seed)
	elif (method.lower() == 'lr'):
		method_func = LogisticRegression(max_iter = 1000, random_state = seed, n_jobs = 1)
	return method_func

def get_method2(method, seed, n_jobs_method):
	if (method.lower() == 'knn'):
		method_func = KNeighborsClassifier(n_jobs = n_jobs_method)
	elif (method.lower() == 'rf'): 
		method_func = RandomForestClassifier(random_state = seed, n_jobs = n_jobs_method)
	elif (method.lower() == 'svm'):
		method_func = svm.SVC(random_state = seed)
	elif (method.lower() == 'lr'):
		method_func = LogisticRegression(max_iter = 1000, random_state = seed, n_jobs = n_jobs_method)
	return method_func

def get_dim_reduction(pca, mv):
	if ((pca == None) & (mv != None)): 
		dr = 'most variable'
		dim_red_func = d_most_variable()
	elif ((pca != None) & (mv == None)):
		dr = 'pca'
		dim_red_func = d_pca(pca)
	else:
		dr = 'None'
		dim_red_func = None
	return dr,dim_red_func

def get_sampling(sampling, seed):
	if (sampling == None):
		samp = 'no sampling'
		sampling_func = None
	elif (sampling.lower() == 'smote'):
		samp = 'SMOTE'
		sampling_func = SMOTE()
	elif (sampling.lower() == 'under'):
		samp = 'RandomUnderSampling'
		sampling_func = RandomUnderSampler(random_state = seed)
	else: 
		samp = 'no sampling'
		sampling_func = None
	return samp,sampling_func

### SKLEARN PIPELINE TO RUN THE NESTED CROSS VALIDATION

def use_pipeline(data, cv_inner, cv_outer, sampling, pca, mv, parameter, method, seed, dataset, model, n_jobs = 10):
	samp, sampling_func = get_sampling(sampling, seed) ### GET THE SPECIFIC SAMPLING PARAMETER
	dr, dim_red_func = get_dim_reduction(pca, mv) ### GET SPECIFIC PARAMETER FOR DIMENSION REDUCTION WITH PCA OR MV
	method_func = get_method(method, seed) ### GET THE SPECIFIC METHOD TO BE USED WITH A SEED
	pipeline = Pipeline([('dim reduction: ' + dr, dim_red_func),('sampling: ' + samp, sampling_func),(method.lower(), method_func)]) ### CREATE THE SKLEARN PIPELINE
	data_X, data_y = get_Xy(data) ### SPLIT FEATURES AND TARGET VARIABLES
	
	in_cv = GridSearchCV(pipeline, parameter, scoring = 'balanced_accuracy', n_jobs = n_jobs, cv = cv_inner) ### INNER CV
	scores_outer = cross_validate(in_cv, data_X, data_y, cv = cv_outer, scoring = 'balanced_accuracy', verbose = 2, n_jobs = n_jobs,  pre_dispatch = 63, return_estimator = True) ### OUTER CV
	### DETERMINE THE BEST MODEL
	best_models = [[k.best_estimator_,k.best_score_, k.best_params_] for k in scores_outer['estimator']]
	outer_scores = [k.best_score_ for k in scores_outer['estimator']]
	best_model, best_average_score, best_model_params= max(best_models, key=(lambda x: x[1]))
	best_params = dict((k,[v] if v else '') for k,v in best_model_params.items())
	final_model = GridSearchCV(best_model, best_params, cv=cv_inner, n_jobs = -1)
	predictions = cross_val_predict(best_model, X =data_X, y = data_y, cv = cv_inner, n_jobs = n_jobs)

	print('Best parameter choice for this {model}: \n\t{params}'
		'\n with a average score of: `{best_average_score}`'.format(
		model = model, params=best_params, best_average_score = best_average_score))

	return {'Set': dataset+'_'+method+'_'+model, 'CV_MODEL': final_model, 'best_model': best_model, 'best_score': best_average_score,'Outer_Scores': outer_scores, 'predictions':predictions}



